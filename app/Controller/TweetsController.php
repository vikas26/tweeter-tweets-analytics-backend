<?php 

class TweetsController extends AppController {
    public $uses = array('Tweet');

    function beforeFilter() {
        parent::beforeFilter();
    }

    /**
     * for fetching all the tweets
     */
    public function daily() {
        // set page information
        $data = array(
            'page' => array(
                'pagetitle' => 'tweetsIndex',
                'title' => 'Daily tweets'
            )
        );

        // conditions for fetching tweets
        $conditions = array(
            'offset' => 0,
            'limit'  => 20,
        );

        // get all tweets
        $data['dailyTweets'] = $this->Tweet->getDailyTweetsCount($conditions);

        // return data
        return json_encode($data);
    }

    /**
     * for fetching all the tweets
     */
    public function weekly() {
        // set page information
        $data = array(
            'page' => array(
                'pagetitle' => 'tweetsIndex',
                'title' => 'Weekly tweets'
            )
        );

        // conditions for fetching tweets
        $conditions = array(
            'offset' => 0,
            'limit'  => 10,
        );

        // get all tweets
        $data['weeklyTweets'] = $this->Tweet->getWeeklyTweetsCount($conditions);

        // return data
        return json_encode($data);
    }

    /**
     * for fetching all the tweets by location
     */
    public function location() {
        // set page information
        $data = array(
            'page' => array(
                'pagetitle' => 'tweetsIndex',
                'title' => 'Tweets by location'
            )
        );

        // conditions for fetching tweets
        $conditions = array(
            'offset' => 0,
            'limit'  => 100,
        );

        // get all tweets
        $data['tweets'] = $this->Tweet->getTweetsByLocation($conditions);

        // return data
        return json_encode($data);
    }

    /**
     * for fetching all the tweets by text
     */
    public function search() {
        // set page information
        $data = array(
            'page' => array(
                'pagetitle' => 'tweetsIndex',
                'title' => 'Tweets by location'
            ),
            'tweets' => array()
        );

        // request params
        $params = json_decode(file_get_contents('php://input'),true);

        // collect post data
        $q          = $params['q'];
        $startDate  = array_key_exists('startDate', $params) ? $params['startDate'] : null;
        $lastDate   = array_key_exists('lastDate', $params) ? $params['lastDate'] : null;

        // serahc text not available
        if (!isset($q) || strlen($q) < 1) {
            return json_encode($data);
        }

        // conditions for fetching tweets
        $conditions = array(
            'q'         => $q,
            'offset'    => 0,
            'startDate' => $startDate,
            'lastDate'  => $lastDate,
        );

        // get all tweets
        $data['tweets'] = $this->Tweet->getTweetsWithKeyword($conditions);

        // return data
        return json_encode($data);
    }
}

?>