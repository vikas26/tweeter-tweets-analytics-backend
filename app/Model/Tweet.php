<?php

class Tweet extends AppModel {

    public $useTable = 'tweets';
    
    /**
     * Called from TweetsController - daily
     *
     * @return json of all tweets
     */
    public function getDailyTweetsCount($data) {
        $options = array();
        $options['fields'] = array(
            'Date(Tweet.tweeted_at) as tweeted_on',
            'count(Tweet.id) as tweets_count',
        );
        $options['order']   = 'Tweet.tweeted_at DESC';
        $options['group']   = 'Date(Tweet.tweeted_at) DESC';
        $options['limit']   = $data['limit'];
        $options['offset']  = $data['offset'];

        return $this->find('all', $options);
    }

    /**
     * Called from TweetsController - weekly
     *
     * @return json of all tweets
     */
    public function getWeeklyTweetsCount($data) {
        $options = array();
        $options['fields'] = array(
            'str_to_date(concat(yearweek(Tweet.tweeted_at), " monday"), "%X%V %W") as date',
            'count(Tweet.id) as tweets_count',
        );
        $options['order']   = 'Tweet.tweeted_at DESC';
        $options['group']   = 'yearweek(Tweet.tweeted_at) DESC';
        $options['limit']   = $data['limit'];
        $options['offset']  = $data['offset'];

        return $this->find('all', $options);
    }

    /**
     * Called from TweetsController - location
     *
     * @return json of all tweets
     */
    public function getTweetsByLocation($data) {
        $options = array();
        $options['fields'] = array(
            'Tweet.location',
            'count(Tweet.id) as tweets_count',
        );
        $options['order']   = 'tweets_count DESC';
        $options['group']   = 'Tweet.location';
        $options['limit']   = $data['limit'];
        $options['offset']  = $data['offset'];

        return $this->find('all', $options);
    }


    /**
     * Called from TweetsController - search
     *
     * @return json of all tweets
     */
    public function getTweetsWithKeyword($data) {
        $options = array();
        $options['fields'] = array(
            'Tweet.tweet',
            'Tweet.tweeted_at',
            'Tweet.tweeted_by',
        );
        $options['conditions'] = array(
            'MATCH(Tweet.tweet, Tweet.tweeted_by)
            AGAINST("*'.$data['q'].'*" IN BOOLEAN MODE)'
        );

        if ($data['startDate']) {
            array_push($options['conditions'], 'Tweet.tweeted_at > "'.$data['startDate'].'"');
        }
        if ($data['lastDate']) {
            array_push($options['conditions'], 'Tweet.tweeted_at < "'.$data['lastDate'].'"');
        }

        $options['order']   = 'Tweet.tweeted_at DESC';
        $options['offset']  = $data['offset'];

        $data = $this->find('all', $options);
        return $data;
    }
}

?>